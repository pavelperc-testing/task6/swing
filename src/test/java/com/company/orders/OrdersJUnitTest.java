package com.company.orders;

import org.assertj.swing.core.matcher.DialogMatcher;
import org.assertj.swing.core.matcher.JButtonMatcher;
import org.assertj.swing.data.TableCell;
import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.edt.GuiQuery;
import org.assertj.swing.fixture.DialogFixture;
import org.assertj.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;


public class OrdersJUnitTest {
    private FrameFixture window;
    
    @BeforeClass
    public static void setUpOnce() {
        FailOnThreadViolationRepaintManager.install();
    }
    
    
    @Before
    public void setUp() {
        final JfrmMain frame = GuiActionRunner.execute(new GuiQuery<JfrmMain>() {
            @Override
            protected JfrmMain executeInEDT() throws Throwable {
                return new JfrmMain();
            }
        });
        
        window = new FrameFixture(frame);
        window.show(); // shows the frame to test
    }
    
    private enum Card {
        VISA("jrbVisa"),
        MASTER("jrbMaster"),
        EXPRESS("jrbExpress");
        
        String rbName;
        
        Card(String rbName) {
            this.rbName = rbName;
        }
    }
    
    private enum Product {
        MyMoney, FamilyAlbum, ScreenSaver
    }
    
    private void fillOrder(String name,
                           Product product,
                           Integer quantity,
                           String date,
                           String street,
                           String city,
                           String state,
                           String zip,
                           Card card,
                           String cardNumber,
                           String expirationDate) {
        // Menu Item Selection
        window.menuItemWithPath("Orders", "New order...").click();
        
        // Filling an order
        final DialogFixture dialog = window.dialog();
        dialog.textBox("jtfName").setText(name);
        dialog.comboBox("jcbProduct").selectItem(product.toString());
//        dialog.spinner("jspQuantity").enterText(quantity.toString());
        
        // works faster
        if (quantity < 1) {
            dialog.spinner("jspQuantity").decrement(-quantity + 1);
        } else if (quantity > 1) {
            dialog.spinner("jspQuantity").increment(quantity - 1);
        }
        
        
        dialog.textBox("jftfDate").setText(date);
        dialog.textBox("jftfDate").setText(date);
        dialog.textBox("jtfStreet").setText(street);
        dialog.textBox("jtfCity").setText(city);
        dialog.textBox("jtfState").setText(state);
        dialog.textBox("jtfZip").setText(zip);
        
        for (Card card1 : Card.values()) {
            dialog.radioButton(card1.rbName).uncheck();
        }
        dialog.radioButton(card.rbName).check();
        dialog.textBox("jtfCardNumber").setText(cardNumber);
        
        dialog.textBox("jftfExpiration").setText(expirationDate);
        dialog.button(JButtonMatcher.withText("Ok")).click();
        
    }
    
    private void fillFromArray(Object[] row) {
//        String[] keys =  {"name", "product", "quantity", "date", "street", "city", "state", "zip"};
        fillOrder(
                (String) row[0],
                (Product) row[1],
                (Integer) row[2],
                (String) row[3],
                (String) row[4],
                (String) row[5],
                (String) row[6],
                (String) row[7],
                (Card) row[8],
                (String) row[9],
                (String) row[10]
        );
    }
    
    private void enableBugs() {
        window.menuItemWithPath("Orders", "Work with bugs").click();
        
    }
    
    
    // Complex test
    @Test
    public void testOrders() {
        Object[][] inputTable = {
                {"Name1", Product.FamilyAlbum, 1, "001/1/01", "street1", "city1", "state1", "zip1", Card.VISA, "card1", "1/1/01"},
                {"Name2", Product.MyMoney, 25, "1/1/002", "street2", "city2", "state2", "zip2", Card.VISA, "card2", "1/1/02"},
                {"Name3", Product.ScreenSaver, 0, "1/1/03", "street3", "city3", "state3", "zip3", Card.VISA, "card3", "1/1/03"},
        };
        
        String[][] outputTable = {
                {"Name1", Product.FamilyAlbum.toString(), "1", "1/1/01", "street1", "city1", "state1", "zip1", Card.VISA.toString(), "card1", "1/1/01"},
                {"Name2", Product.MyMoney.toString(), "25", "1/1/02", "street2", "city2", "state2", "zip2", Card.VISA.toString(), "card2", "1/1/02"},
                {"Name3", Product.ScreenSaver.toString(), "0", "1/1/03", "street3", "city3", "state3", "zip3", Card.VISA.toString(), "card3", "1/1/03"},
                {"Name3", Product.ScreenSaver.toString(), "0", "1/1/03", "street3", "city3", "state3", "zip3", Card.VISA.toString(), "card3", "1/1/03"},
        };

//        window.menuItemWithPath("File", "New").click();
        
        for (Object[] row : inputTable) {
            fillFromArray(row);
        }
        // dublicate last row
        fillFromArray(inputTable[inputTable.length - 1]);
        
        for (int i = 0; i < outputTable.length; i++) {
            for (int j = 0; j < outputTable[i].length; j++) {
                // Checking table contents
                window.table().requireCellValue(
                        TableCell.row(i).column(j),
                        outputTable[i][j]
                );
            }
        }
        
        // Checking table contents
        window.table().requireCellValue(
                TableCell.row(0).column(0),
                "Name1"
        );
        
        // remove the first row
        window.table().cell(TableCell.row(0).column(0)).click();
        window.menuItemWithPath("Orders", "Delete order").click();
        window.dialog(DialogMatcher.withTitle("Confirmation")).button(JButtonMatcher.withText("Yes")).click();
    
    
        // edit the third row
        window.table().cell(TableCell.row(2).column(0)).click();
        window.menuItemWithPath("Orders", "Edit order...").click();
        window.dialog().textBox("jtfName").setText("Edited");
        window.dialog().button(JButtonMatcher.withText("Ok")).click();
    
    
        outputTable = new String[][]{
                {"Name2", Product.MyMoney.toString(), "25", "1/1/02", "street2", "city2", "state2", "zip2", Card.VISA.toString(), "card2", "1/1/02"},
                {"Name3", Product.ScreenSaver.toString(), "0", "1/1/03", "street3", "city3", "state3", "zip3", Card.VISA.toString(), "card3", "1/1/03"},
                {"Edited", Product.ScreenSaver.toString(), "0", "1/1/03", "street3", "city3", "state3", "zip3", Card.VISA.toString(), "card3", "1/1/03"},
        };
        
        for (int i = 0; i < outputTable.length; i++) {
            for (int j = 0; j < outputTable[i].length; j++) {
                // Checking table contents
                window.table().requireCellValue(
                        TableCell.row(i).column(j),
                        outputTable[i][j]
                );
            }
        }
    }
    
    
    @Test
    public void testOrdersWithBugs() {
        enableBugs();
        testOrders();
    }
    
    private void checkTotalSum(DialogFixture dialog) {
        // \u00A0 is a special symbol "non breaking space" inside the number
        int quantity = Integer.parseInt(dialog.spinner("jspQuantity").text().replace("\u00A0", ""));
        int price = Integer.parseInt(dialog.textBox("jtfPrice").text().substring(1));
        int discount = Integer.parseInt(dialog.textBox("jtfDiscount").text().replace("%", ""));
        int total = Integer.parseInt(dialog.textBox("jtfTotal").text().substring(1));

//        System.out.println("quantity = " + quantity);
//        System.out.println("price = " + price);
//        System.out.println("discount = " + discount);
//        System.out.println("total = " + total);
        
        assertEquals(total, quantity * price * (100 - discount) / 100);
    }
    
    @Test
    public void testDiscount() {
        window.menuItemWithPath("Orders", "New order...").click();
        
        final DialogFixture dialog = window.dialog();
        
        
        for (Product product : Product.values()) {
            // setup
            dialog.spinner("jspQuantity").enterTextAndCommit("1");
            dialog.comboBox("jcbProduct").selectItem(product.toString());
            
            checkTotalSum(dialog);
            
            dialog.spinner("jspQuantity").decrement();
            checkTotalSum(dialog);
            
            dialog.spinner("jspQuantity").increment(10);
            checkTotalSum(dialog);
            
            dialog.spinner("jspQuantity").increment(100);
            checkTotalSum(dialog);
            
            dialog.spinner("jspQuantity").increment(1000);
            checkTotalSum(dialog);
        }
    }
    
    @Test
    public void testDiscountWithBugs() {
        enableBugs();
        testDiscount();
    }
    
    @After
    public void tearDown() {
        window.cleanUp();
    }
}